package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pojo.Singer;
import utils.DBUtils;
public class SingerDao implements Closeable{
	Connection connection = null;
	CallableStatement stmtInsert;
	CallableStatement stmtUpdate;
	CallableStatement stmtDelete;
	CallableStatement stmtSelect;
	public SingerDao() throws Exception{
		 this.connection = DBUtils.getConnection();
		 this.stmtInsert = this.connection.prepareCall("{call sp_insert_singer(?,?,?,?,?)}");
		 this.stmtUpdate = this.connection.prepareCall("{call sp_update_singer(?,?)}");
		 this.stmtDelete = this.connection.prepareCall("{call sp_delete_singer(?)}");
		 this.stmtSelect = this.connection.prepareCall("{call sp_select_singer()}");
	}
	public int insert(Singer singer) throws Exception{
		this.stmtInsert.setInt(1, singer.getName());
		this.stmtInsert.setString(2, singer.getGender());
		this.stmtInsert.setString(3, singer.getAge());
		this.stmtInsert.setString(4, singer.getEmail());
		this.stmtInsert.setFloat(5, singer.getContact());
		this.stmtInsert.setFloat(5, singer.getRating());
		this.stmtInsert.execute();
		return stmtInsert.getUpdateCount();
	}
	public int update(int rating) throws Exception{
		
		this.stmtUpdate.setInt(7);
		this.stmtUpdate.execute();
		return stmtUpdate.getUpdateCount();
	}
	public int delete() throws Exception{
		this.stmtDelete.setInt(1);
		this.stmtDelete.execute();
		return stmtDelete.getUpdateCount();
	}
	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
	}
}